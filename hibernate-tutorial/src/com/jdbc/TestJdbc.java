package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mysql://localhost:3306/hb_student_tracker?useSSL=false&server"
				+ "Timezone=UTC";
		String user = "hbstudent";
		String password = "hbstudent";
		
		try {
		System.out.println("connecting to database " + jdbcUrl);
		Connection myConn = DriverManager.getConnection(jdbcUrl, user, password);
		System.out.println("success!");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
