<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 


<!Doctype html>

<html>

<head>
	<title>Student Registration Form</title>
</head>

<body>

	<form:form action="processForm" modelAttribute="student">
	
	First name: <form:input path="firstName" />
	
	<br></br>
	
	Last name: <form:input path="lastName" />
	
	<br></br>
	
	Country:
	 <form:select path="country"> 
 	<form:options items="${theCountryOptions}" />
	</form:select>

	<br><br>
	
	Favorite language:
	Java <form:radiobutton path="favoriteLanguage" value="Java" />
	PHP <form:radiobutton path="favoriteLanguage" value="PHP" />
	C# <form:radiobutton path="favoriteLanguage" value="C#" />
	Ruby <form:radiobutton path="favoriteLanguage" value="Ruby" />
	
	<br><br>
	
	Linux <form:checkbox path="operatingSystem" value="Linux" />
	MSWindows <form:checkbox path="operatingSystem" value="MSWindows" />
	MacOS <form:checkbox path="operatingSystem" value="MacOS" />
	
	<br><br>
	

	<input type="submit" Value="Submit" />

	</form:form>
	
	
	
</body>



</html>

