package com.springdemo;

public class SoccerCoach implements Coach {
	//define a private field for the dependency
	private FortuneService fortuneService;
	private String emailAddress;
	private String teamName;
			
	public SoccerCoach() {
		System.out.println("SoccerCoach: inside no arg constructor");
	}
	//define a constructor for dependency injection
	public SoccerCoach(FortuneService theFortuneService) {
				fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "ball drills for atleast 30 minutes";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("setting fortuneService");
		this.fortuneService = fortuneService;
	}
	
	public void setEmailAddress(String emailAdress) {
		System.out.println("inside setter email");
		this.emailAddress = emailAdress;
	}
	
	public String getEmailAdress() {
		return emailAddress;
	}
	
	public void setTeamName(String teamName) {
		System.out.println("inside set team name");
		this.teamName = teamName;
	}
	
	public String getTeamName() {
		return teamName;
	}

}
