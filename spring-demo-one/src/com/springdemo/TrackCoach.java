package com.springdemo;

public class TrackCoach implements Coach {
	//define a private field for the dependency
	private FortuneService fortuneService;
	public TrackCoach() {
		
	}
		//define a constructor for dependency injection
	public TrackCoach(FortuneService theFortuneService) {
			fortuneService = theFortuneService;
	}
	@Override
	public String getDailyWorkout() {
		return "Run around the building 10 times";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	
	public void startup() {
		System.out.println("intialized startup");
	}
	
	public void cleanup() {
		System.out.println("cleanup");
	}

}
