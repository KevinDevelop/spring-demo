package com.springdemo;

import java.util.Random;

public class HappyFortuneService implements FortuneService {

	String[] fortuneArray = {"Good day", "Bad day", "Inbetween day"};
	
	@Override
	public String getFortune() {
		Random rand = new Random();
		int randNum = rand.nextInt(3);
		return fortuneArray[randNum];
	}

}
