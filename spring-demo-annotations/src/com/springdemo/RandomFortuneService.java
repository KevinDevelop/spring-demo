package com.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {

	String[] randomStrings = {"you are the best", "today will be bright", "good luck"};
	private Random myRandom = new Random();
	
	@Override
	public String getFortune() {
		int index = myRandom.nextInt(randomStrings.length);
		String fortune = randomStrings[index];
		return fortune;
	}

}
