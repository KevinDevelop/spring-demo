package com.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {
	
	@Qualifier("randomFortuneService")
	private FortuneService fortuneService;
	/*
	@Autowired
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	*/
	public TennisCoach() {
		System.out.println("no arg constructor tennis coach");
	}
	@Override
	public String getDailyWorkout() {
		return "target practice for 15 minutes";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	/*
	@Autowired
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("inside set fortune service");
		this.fortuneService = fortuneService;
	
	}
	*/
}
