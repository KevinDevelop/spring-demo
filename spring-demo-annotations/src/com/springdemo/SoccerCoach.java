package com.springdemo;

import org.springframework.stereotype.Component;

@Component

public class SoccerCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public SoccerCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "run 1000 laps and do 1000 situps";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
