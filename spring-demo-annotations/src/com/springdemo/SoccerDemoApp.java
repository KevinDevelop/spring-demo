package com.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SoccerDemoApp {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(SportConfig.class);
			
			// get the bean from spring container
			SoccerCoach swimCoach = context.getBean("soccerCoach", SoccerCoach.class);
			
			//call a method on the bean
			System.out.println(swimCoach.getDailyWorkout());		
			System.out.println(swimCoach.getDailyFortune());
//			System.out.println("Email: " + swimCoach.getEmail());
//			System.out.println("Team: " + swimCoach.getTeam());
			
			//close the context
			context.close();

	}

}
